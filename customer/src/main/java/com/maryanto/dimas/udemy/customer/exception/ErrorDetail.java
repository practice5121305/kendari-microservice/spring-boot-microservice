package com.maryanto.dimas.udemy.customer.exception;

import lombok.Data;

@Data
public class ErrorDetail {

    private boolean status;
    private String message;

    public ErrorDetail(boolean status, String message) {

        super();

        this.status = status;

        this.message = message;

    }

}
