package com.maryanto.dimas.udemy.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IdNotFoundException.class)
    public ResponseEntity<ErrorDetail> idNotFoundException(IdNotFoundException ex){
        ErrorDetail errorModel = new ErrorDetail(false, ex.getMessage());
        return new ResponseEntity<ErrorDetail>(errorModel, HttpStatus.NOT_FOUND);
    }

}
