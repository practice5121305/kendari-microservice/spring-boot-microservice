package com.maryanto.dimas.udemy.customer.controller;

import com.maryanto.dimas.udemy.customer.entity.Customer;
import com.maryanto.dimas.udemy.customer.repository.CustomerRepository;
import com.maryanto.dimas.udemy.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@RestController
@RequestMapping("/api/customer/v1")
public class CustomerController {

    private CustomerRepository repo;
    private CustomerService service;

    @Autowired
    public CustomerController(CustomerRepository repo, CustomerService service) {
        this.repo = repo;
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Customer> save(
            @RequestBody Customer customer
    ){
        Customer result = this.repo.save(customer);
        return ResponseEntity.ok(result);
    }

    @PutMapping
    public ResponseEntity<?> update(
            @RequestParam("file") MultipartFile file,
            @RequestParam("id") String id
    ) throws Exception {
        Customer customer = this.service.update(id, file);
        return ResponseEntity.ok(customer);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Customer> findById(@PathVariable("id") String id) {
        Optional<Customer> optional = this.repo.findById(id);
        return optional.map(ResponseEntity::ok).orElseGet(() ->
                ResponseEntity.noContent().build());
    }

    @GetMapping("/findByUserId/{id}")
    public ResponseEntity<Customer> findByUserId(@PathVariable("id") String id) {
        Optional<Customer> optional = this.repo.findFirstByUserId(id);
        return optional.map(ResponseEntity::ok).orElseGet(() ->
                ResponseEntity.noContent().build());
    }
}
