package com.maryanto.dimas.udemy.customer.service;

import com.maryanto.dimas.udemy.customer.entity.Customer;
import com.maryanto.dimas.udemy.customer.exception.IdNotFoundException;
import com.maryanto.dimas.udemy.customer.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository repo;
    private final MinioService minioService;

    public Customer update(String id, MultipartFile file) throws Exception {
        Customer customer = this.repo.findById(id).orElseThrow(() ->
                new IdNotFoundException("Customer dengan id " + id + " tidak ditemukan"));

        Path path = Path.of(file.getOriginalFilename());

        String imageUrl = minioService.saveFile(path, file.getInputStream(), "bucket");
        log.info("image url ==> " + imageUrl);

        customer.setImageUrl(imageUrl);

        Customer updatedCustomer = this.repo.save(customer);
        return updatedCustomer;
    }

}
